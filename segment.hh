#ifndef SEGMENT_HH
#define SEGMENT_HH

#include <glm/vec2.hpp>
#include <SDL2/SDL.h>


/**
* @brief presentation of line between two points
*/
class Segment {

public:
    Segment(const glm::vec2 &begin, const glm::vec2 &end);
    ~Segment();


    /**
    * @brief draw line between begin and end, draw small rectangles at points
    *
    * @param r, SDL rendering context
    */
    void draw(SDL_Renderer* r);    

    glm::vec2 begin;
    glm::vec2 end;

};


#endif
