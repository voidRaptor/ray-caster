#ifndef UTILITY_HH
#define UTILITY_HH

#include <vector>
#include <string>


/**
 * @brief collection of different utility functions
 */
namespace Utility {

    
/**
 * @brief map, map value from certain range to another 
 * 
 * @param min1, minimum of original range 
 * @param max1, maximum of original range 
 * @param min2, minum of second range 
 * @param max2, maximum of second range
 * @return mapped value
 */
float map(const float value,
          const float min1, const float max1,
          const float min2, const float max2);


/**
 * @brief random, return random number, [min, max]
 */
int random(const int &min, const int &max);


/**
 * @brief split, split string by separator into vector of strings
 * 
 * @param vec, storage for split strings 
 * @param s, string to split 
 * @param separator, string which determines where to split 
 * @param removeEmpty, whether empty string will be removed
 * @param removeSpaces, whether spaces will be removed 
 */
void split(std::vector<std::string> &vec,
           std::string s,
           const std::string &separator,
           bool removeEmpty=true,
           bool removeSpaces=false);




/**
* @brief calculate number of digits in a number
*
* @param x, number
*
* @return digits
*/
int digits(int x);  

    
};

#endif
