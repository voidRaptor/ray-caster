#include "keyboardinput.hh"


KeyboardInput* KeyboardInput::getInstance() {
    static KeyboardInput instance;
    return &instance;
}


KeyboardInput::KeyboardInput() {

}


bool KeyboardInput::exitPressed() {
    
    while ( SDL_PollEvent(&mEvent) ) {

        if (mEvent.type == SDL_QUIT || mEvent.key.keysym.sym == SDLK_q ) {
            return true;
        }
        
    }

    return false;
}


void KeyboardInput::handleMovement(glm::vec2* v) {
    SDL_PumpEvents();
    const Uint8* keyStates = SDL_GetKeyboardState(NULL);
    
    if ( keyStates[SDL_SCANCODE_W] )      v->y = -1.0f;
    else if ( keyStates[SDL_SCANCODE_S] ) v->y =  1.0f;
    else                                  v->y =  0.0f;
    
    if ( keyStates[SDL_SCANCODE_A] )      v->x = -1.0f;
    else if ( keyStates[SDL_SCANCODE_D] ) v->x =  1.0f; 
    else                                  v->x =  0.0f;    
}


void KeyboardInput::handleCamera(float &angle) {
    SDL_PumpEvents();
    const Uint8* keyStates = SDL_GetKeyboardState(NULL);
    
/*
    if ( keyStates[SDL_SCANCODE_I] )      v->y = -1.0f;
    else if ( keyStates[SDL_SCANCODE_K] ) v->y =  1.0f;
    else                                  v->y =  0.0f;
*/
    static const float ANGLE = 0.01f;
    if ( keyStates[SDL_SCANCODE_J] )      angle -= ANGLE;
    else if ( keyStates[SDL_SCANCODE_L] ) angle += ANGLE;
}
