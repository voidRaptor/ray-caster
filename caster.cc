#include "caster.hh"
#include "keyboardinput.hh"
#include "utility.hh"
#include <glm/gtc/constants.hpp>
#include <iostream>
#include <set>
#include <algorithm>
#include <SDL2/SDL2_gfxPrimitives.h>


Caster::Caster(const glm::vec2 &startPos):
    mPos(startPos) {

        init();
}


void Caster::update() {
    glm::vec2 movement;
    KeyboardInput::getInstance()->handleMovement(&movement);     

    mPos += movement * 1.0f;
}


void Caster::draw(SDL_Renderer* r, bool debugMode) {
    int x1 = 0;
    int y1 = 0;
    int x2 = 0;
    int y2 = 0;
    int x3 = 0;
    int y3 = 0;

    // draw triangles from global points (center, lhs, rhs)
    for (unsigned int i=1; i<mIntersections.size(); ++i) {
        x1 = mIntersections[i-1].point.x; 
        y1 = mIntersections[i-1].point.y; 
        x2 = mIntersections[i].point.x; 
        y2 = mIntersections[i].point.y; 
        x3 = mPos.x;
        y3 = mPos.y;

        filledTrigonColor(r, x1, y1, x2, y2, x3, y3, 0xFFFFFFFF);

        if (debugMode) {
            SDL_SetRenderDrawColor(r, 0, 255, 0, 255);
            SDL_RenderDrawLine(r, x1, y1, x2, y2);
            SDL_RenderDrawLine(r, x2, y2, x3, y3);
            SDL_RenderDrawLine(r, x3, y3, x1, y1);
        }

    }

    // final triangle
    x1 = mIntersections[0].point.x; 
    y1 = mIntersections[0].point.y; 
    x2 = mIntersections.back().point.x; 
    y2 = mIntersections.back().point.y; 
    x3 = mPos.x;
    y3 = mPos.y;

    filledTrigonColor(r, x1, y1, x2, y2, x3, y3, 0xFFFFFFFF);


    if (debugMode) {
        SDL_SetRenderDrawColor(r, 0, 255, 0, 255);
        SDL_RenderDrawLine(r, x1, y1, x2, y2);
        SDL_RenderDrawLine(r, x2, y2, x3, y3);
        SDL_RenderDrawLine(r, x3, y3, x1, y1);
    }
    // circle at eye of the beholder
    filledCircleColor(r, mPos.x, mPos.y, 3, 0xFF0000FF);

}


void Caster::cast(std::vector<Obstacle> &obstacles, 
                  const int screenWidth, 
                  const int screenHeight) {

    // OPTIMIZATIONS:
    // - https://www.gamedev.net/forums/topic/702108-large-tilemap-storage/5405321/
    // - tiles, chunks (64x64?), stacks (64x64?) / quadtree

    mIntersections.clear(); 
    std::vector<glm::vec2> endPoints;

    // TODO: remove vsync to see how slow it is to get points each frame
    getUniquePoints(obstacles, endPoints);

    std::vector<float> uniqueAngles;

    for (unsigned int i=0; i<endPoints.size(); ++i) {
       
        // limit points to fit screen
        if (endPoints[i].x < 0.0f) endPoints[i].x = 0.0f;
        if (endPoints[i].y < 0.0f) endPoints[i].y = 0.0f;
        if (endPoints[i].x > screenWidth)  endPoints[i].x = screenWidth;
        if (endPoints[i].y > screenHeight) endPoints[i].y = screenHeight;

        float angle = atan2(endPoints[i].y - mPos.y, endPoints[i].x - mPos.x);

        static const float OFFSET = 0.0001;
        uniqueAngles.push_back(angle - OFFSET);
        uniqueAngles.push_back(angle);
        uniqueAngles.push_back(angle + OFFSET);
    }

    std::vector<Segment> segs;

    for (unsigned int i=0; i<obstacles.size(); ++i) {
        std::vector<Segment> *s = obstacles[i].getSegments();

        for (unsigned int j=0; j<s->size(); ++j) {
            segs.push_back( s->at(j) );
        }
    }

    for (unsigned int i=0; i<uniqueAngles.size(); ++i) {
        glm::vec2 rayEnd = {cos(uniqueAngles[i]), sin(uniqueAngles[i])};

        glm::vec2 closestIntersection;
        float record = FLT_MAX;

        for (unsigned int j=0; j<segs.size(); ++j) {
            glm::vec2 point;
            float u = -1.0f;

            if ( !intersect(mPos, rayEnd, segs[j], &point, &u) ) continue;

            // is closer
            if (0.0f <= u && u < record) {
                closestIntersection = point;
                record = u;
            }


        }

        // no intersection
        if (record == FLT_MAX) continue;

        mIntersections.push_back({closestIntersection, uniqueAngles[i]});

    }

    if (mIntersections.size() <= 1) return;

    std::sort(mIntersections.begin(), 
              mIntersections.end(), 
              [](const auto &a, const auto &b) { return a.angle < b.angle; });
    
    std::cout << "unique angles: " << uniqueAngles.size()
              << ", segs: " << segs.size()
              << ", intersection count: " << mIntersections.size() << "\n";

}


void Caster::cast2(SDL_Renderer* r, std::vector<Obstacle> &obstacles) {

/*
    std::vector<Intersection> rays;
    std::vector<glm::vec2> endPoints;

    // TODO: remove vsync to see how slow it is to get points each frame
    getUniquePoints(obstacles, endPoints);
    std::vector<Segment> segs;

    for (unsigned int i=0; i<obstacles.size(); ++i) {
        std::vector<Segment> *s = obstacles[i].getSegments();

        for (unsigned int j=0; j<s->size(); ++j) {
            segs.push_back( s->at(j) );
        }
    }

    // extra rays for reaching mid segment instead of stopping at corners
    unsigned int max = endPoints.size();
    for (unsigned int i=0; i<1; ++i) {
        glm::vec2 end = {endPoints[i].x - mPos.x, endPoints[i].y - mPos.y};

        float angle = atan2(end.y, end.x);

        float rhsAngle = angle + 0.000001f;
        glm::vec2 rhsEnd;
        rhsEnd.x = mPos.x + cos(rhsAngle) * end.x;
        rhsEnd.y = mPos.y + sin(rhsAngle) * end.y;
        endPoints.push_back(rhsEnd);

        float lhsAngle = angle - 0.000001f;
        glm::vec2 lhsEnd;
        lhsEnd.x = mPos.x + cos(lhsAngle) * end.x;
        lhsEnd.y = mPos.y + sin(lhsAngle) * end.y;
        endPoints.push_back(lhsEnd);

    }


    for (unsigned int i=0; i<endPoints.size(); ++i) {
        Ray ray(mPos, endPoints[i]-mPos);
        glm::vec2 closestIntersection;
        float record = FLT_MAX;

        // TODO: if closest intersection is endpoint, continue ray
        for (unsigned int j=0; j<segs.size(); ++j) {
            glm::vec2 point;
            float u = -1.0f;

            if ( !ray.intersect(segs[j], &point, &u) ) continue;

            // is closer
            if (0.0f <= u && u < record) {
                closestIntersection = point;
                record = u;
            }


        }

        // no intersection
        if (record == FLT_MAX) continue;

        float angle = atan2(endPoints[i].y - mPos.y, endPoints[i].x - mPos.x);
        rays.push_back({closestIntersection, angle});


    }

    std::sort(rays.begin(), 
              rays.end(), 
              [](const auto &a, const auto &b) { return a.angle < b.angle; });
    
    
    std::cout << "ray count: " << rays.size() << "\n";
    // draw triangles from global points (center, lhs, rhs)
    for (unsigned int i=1; i<rays.size(); ++i) {
        int x1 = rays[i-1].point.x; 
        int y1 = rays[i-1].point.y; 
        int x2 = rays[i].point.x; 
        int y2 = rays[i].point.y; 
        int x3 = mPos.x;
        int y3 = mPos.y;

        //filledTrigonColor(r, x1, y1, x2, y2, x3, y3, 0x00FF00FF);
        SDL_SetRenderDrawColor(r, 0, 255, 0, 255);
        //SDL_RenderDrawLine(r, x1, y1, x2, y2);
        SDL_RenderDrawLine(r, x2, y2, x3, y3);
        SDL_RenderDrawLine(r, x3, y3, x1, y1);


    }

*/
}


void Caster::init() {

}


void Caster::getUniquePoints(std::vector<Obstacle> &obstacles, 
                             std::vector<glm::vec2> &endPoints) {

    for (unsigned int i=0; i<obstacles.size(); ++i) {
        std::vector<Segment> *segs = obstacles[i].getSegments();

        for (unsigned int j=0; j<segs->size(); ++j) {
            // TODO: replace with set 

            glm::vec2 begin = segs->at(j).begin;
            glm::vec2 end = segs->at(j).end;

            auto it = std::find_if(endPoints.begin(), 
                                   endPoints.end(), 
                                   [begin](const auto& p) {
                                        return (begin == p);
                                   });
            
            if ( it == endPoints.end() ) {
                endPoints.push_back(begin);
            }
            
            it = std::find_if(endPoints.begin(), 
                              endPoints.end(), 
                              [end](const auto& p) {
                                   return (end == p);
                              });
            
            if ( it == endPoints.end() ) {
                endPoints.push_back(end);
            }
                                                                               

        }

    }
}


bool Caster::intersect(const glm::vec2 &begin, 
                       const glm::vec2 &end, 
                       const Segment &seg, 
                       glm::vec2 *point, 
                       float *paramU) {

    float x1 = seg.begin.x;
    float y1 = seg.begin.y;
    float x2 = seg.end.x;
    float y2 = seg.end.y;
    
    float x3 = begin.x;
    float y3 = begin.y;
    float x4 = begin.x + end.x;
    float y4 = begin.y + end.y;

    float denominator = (x1-x2) * (y3-y4) - (y1-y2) * (x3-x4);
    *paramU = -1.0f;

    if (denominator == 0.0f) return false;

    float t =  ( (x1-x3) * (y3-y4) - (y1-y3) * (x3-x4) ) / denominator;
    float u = -( (x1-x2) * (y1-y3) - (y1-y2) * (x1-x3) ) / denominator;

    
    if (0.0f <= t && t <= 1.0f && u >= 0.0f) {

        if (point != nullptr) {
            point->x = x1 + t * (x2-x1);
            point->y = y1 + t * (y2-y1);
        }

        if (paramU != nullptr) *paramU = u;
        return true;

    }

    return false;
}
