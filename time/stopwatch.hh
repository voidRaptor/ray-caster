#ifndef STOPWATCH_HH
#define STOPWATCH_HH


#include "itimer.hh"


enum class Mode {HiRes, LowRes};

/**
 * @brief Stopwatch, simple wrapper for std::high_resolution_clock
 * 
 */
class Stopwatch {
    
public:
    Stopwatch(Mode mode=Mode::LowRes, double timeout=-1.0f);
    ~Stopwatch();
    
    void reset();
    void start(double timeout=-1.0f);
    
    /**
     * @brief getTotalTime, return the total time from creating the object 
     */
    double getTotalTime();
    
    /**
     * @brief getDeltaTIme, return elapsed time after previous call to this function
     */
    double getDeltaTime();
    
    /**
     * @brief isTimeout, return true if user set time limit has been reached 
     */
    bool isTimeout();
    
    

private:
    Mode mMode;
    
    ITimer* mTimer;
    double mTimeout;
};


#endif