#ifndef HIRESTIMER_HH
#define HIRESTIMER_HH

#include "itimer.hh"
#include <chrono>

using TimePoint = std::chrono::time_point<std::chrono::high_resolution_clock>;


/**
 * @brief HiResTimer, timer with more precision, uses STL chrono's high_resolution_clock
 */
class HiResTimer: public ITimer {

public:
    HiResTimer();
    virtual ~HiResTimer();
    
    virtual double getTotal();
    virtual double getDelta();
    
    virtual void start();
    
    
private:
    TimePoint mTotalStart;
    TimePoint mDeltaStart;
    
};


#endif