#include "segment.hh"
#include <glm/glm.hpp>

Segment::Segment(const glm::vec2 &begin, const glm::vec2 &end):
    begin(begin), 
    end(end) {


}


Segment::~Segment() {


}


void Segment::draw(SDL_Renderer* r) {
    SDL_SetRenderDrawColor(r, 0, 0, 255, 255);
    SDL_RenderDrawLine(r, begin.x, begin.y, end.x, end.y);

    // draw rectangles at the end points
    static const int WIDTH = 6;
    static const int HEIGHT = 6;

    SDL_Rect rectBegin = {(int)begin.x - WIDTH/2, (int)begin.y - HEIGHT/2, WIDTH, HEIGHT};
    SDL_Rect rectEnd = {(int)end.x - WIDTH/2, (int)end.y - HEIGHT/2, WIDTH, HEIGHT};

    SDL_RenderDrawRect(r, &rectBegin);
    SDL_RenderDrawRect(r, &rectEnd);

    SDL_RenderFillRect(r, &rectBegin);
    SDL_RenderFillRect(r, &rectEnd);
}
