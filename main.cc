#include "window.hh"
#include "time/stopwatch.hh"
#include "keyboardinput.hh"
#include "caster.hh"
#include "obstacle.hh"
#include "utility.hh"
#include <ctime>
#include <algorithm>

static const float MAX_SIZE = 200.0f;
static const float MIN_SIZE = 20.0f;
static const unsigned int COUNT = 1;

// TODO: if ray intersects one segment of obstacle, skip rest of the segs or sort interects
// https://www.redblobgames.com/articles/visibility/
// https://ncase.me/sight-and-light/

int main() {
    Window w;
    srand( time(NULL) );

    KeyboardInput* input = KeyboardInput::getInstance();
    SDL_Renderer* r = w.getRenderer(); 

    std::vector<Obstacle> obs;

    // screen edges
    obs.emplace_back(glm::vec4{w.WIDTH/2.0f, w.HEIGHT/2.0f, w.WIDTH, w.HEIGHT}); 

    obs.emplace_back(glm::vec4{w.WIDTH/4.0f, w.HEIGHT/2.0f, 40.0f, 60.0f}); 
    obs.emplace_back(glm::vec4{0.0f, w.HEIGHT/2.0f, 40.0f, 60.0f}); 

/*
    // other obstacles
    for (unsigned int i=0; i<COUNT; ++i) {
        float x = Utility::random(0, w.WIDTH);
        float y = Utility::random(0, w.HEIGHT);
        float w = Utility::random(MIN_SIZE, MAX_SIZE);
        float h = Utility::random(MIN_SIZE, MAX_SIZE);

        //obs.emplace_back(x, y, w, h);
        //obs.push_back( Obstacle() );
    }
*/

    Caster caster({w.WIDTH / 2.0f, w.HEIGHT / 2.0f});

    Stopwatch frameTimer;
    frameTimer.start();
    unsigned int loops = 0;

    // update loop
    while ( !input->exitPressed() ) {
        w.clearScreen();

        for (unsigned int i=0; i<obs.size(); ++i) {
            obs[i].draw(r);
        }

        caster.update();
        caster.cast(obs, w.WIDTH, w.HEIGHT);
        caster.draw(r, true);

        w.updateScreen();

        if (loops > 50) {
            w.updateFrameCounter( frameTimer.getDeltaTime() / 50.0f );
            loops = 0;
        }

        ++loops;
    }

    return 0;
}


