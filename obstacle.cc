#include "obstacle.hh"
#include "utility.hh"


Obstacle::Obstacle(const glm::vec4& dimensions):
    mCenter({dimensions.x, dimensions.y}),
    mSize({dimensions.z, dimensions.w}) {

        init();
}


Obstacle::Obstacle(const glm::vec2 &center, float radius, unsigned int segments) {
    
    for (unsigned int i=0; i<segments; ++i) {
        // TODO: random shaped with radius, copy e.g. from asteroids 
        


    }

}


Obstacle::Obstacle() {
        initRand();
}


void Obstacle::draw(SDL_Renderer* r) {
    
    for (unsigned int i=0; i<mSegments.size(); ++i) {
        mSegments[i].draw(r);
    }

}


std::vector<Segment>* Obstacle::getSegments() {
    return &mSegments;
}


void Obstacle::init() {

    glm::vec2 topLeft; 
    topLeft.x = mCenter.x - 0.5f * mSize.x;
    topLeft.y = mCenter.y - 0.5f * mSize.y;

    glm::vec2 topRight; 
    topRight.x = mCenter.x + 0.5f * mSize.x;
    topRight.y = mCenter.y - 0.5f * mSize.y;

    glm::vec2 bottomRight; 
    bottomRight.x = mCenter.x + 0.5f * mSize.x;
    bottomRight.y = mCenter.y + 0.5f * mSize.y;

    glm::vec2 bottomLeft; 
    bottomLeft.x = mCenter.x - 0.5f * mSize.x;
    bottomLeft.y = mCenter.y + 0.5f * mSize.y;

    mSegments.emplace_back(topLeft, topRight);
    mSegments.emplace_back(topRight, bottomRight);
    mSegments.emplace_back(bottomRight, bottomLeft);
    mSegments.emplace_back(bottomLeft, topLeft);

}


void Obstacle::initRand() {
    static const float MIN = 100.0f;
    static const float MAX = 400.0f;
    const unsigned int COUNT = Utility::random(2, 5);
    
    std::vector<glm::vec2> points;

    for (unsigned int i=0; i<COUNT; ++i) {
        float x = Utility::random(MIN, MAX);
        float y = Utility::random(MIN, MAX);
        
        points.emplace_back(x, y);
    }

    for (unsigned int i=0; i<points.size()-1; ++i) {
        mSegments.emplace_back(points[i], points[i+1]); 
    }

    mSegments.emplace_back(points.back(), points[0]);
}

