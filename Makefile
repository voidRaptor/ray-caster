OBJS = *.cc time/*.cc

CC = g++

FLAGS = -Wall -g -std=c++14

LIBS = -lSDL2 -lSDL2_gfx

OUT = a.out


all: $(OBJS) ; $(CC) $(FLAGS) $(LIBS) $(OBJS) -o $(OUT)

clean: 
	-rm *.out
