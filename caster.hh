#ifndef CASTER_HH
#define CASTER_HH

#include "obstacle.hh"
#include <vector>
#include <glm/vec2.hpp>


struct Intersection {
    glm::vec2 point; // intersection point
    float angle;     // angle between positive x axis and intersection point
};


/**
* @brief object which casts rays to end points of segments 
*/
class Caster {

public:
    Caster(const glm::vec2 &startPos);


    
    /**
    * @brief update viewer position according to keyboard input
    */
    void update();


    /**
    * @brief draw visibility triangles
    *
    * @param r, SDL rendering context
    * @param debugMode, whether to draw triangle borders
    */
    void draw(SDL_Renderer* r, bool debugMode=false);


    /**
    * @brief cast rays to form visibility triangles
    *
    * @param obstacles, list of obstacles to check for
    * @param screenWidth, screen width
    * @param screenHeight, screen height
    *
    * @note uses algorithm from https://ncase.me/sight-and-light/
    * @note clears and populates mIntersections in order of intersection angle
    */
    void cast(std::vector<Obstacle> &obstacles, 
              const int screenWidth, 
              const int screenHeight);


    /**
    * @brief same functionality as the other cast, but uses different algorithm
    *
    * @param r, SDL rendering context
    * @param obstacles, list of obstacles to check for
    *
    * @note uses modified version of cast1
    * @note NOT USED
    */
    void cast2(SDL_Renderer* r, std::vector<Obstacle> &obstacles);


private:
    void init();



    /**
    * @brief populate vector to contain all end points of the obstacles
    *
    * @param obstacles, list of obstacles to check
    * @param endPoints, container for end points
    */
    void getUniquePoints(std::vector<Obstacle> &obstacles, 
                         std::vector<glm::vec2> &endPoints);


    /**
    * @brief check if ray intersects with a segment
    *
    * @param begin, start point of ray
    * @param end, end point of ray
    * @param seg, segment to check
    * @param point, set as point of intersection if there is any
    * @param paramU, used for finding closest intersection, -1.0 if no intersection
    *
    * @return true if there is intersection
    *
    * @pre addresses of point and paramU are valid
    * @post values are set and valid 
    */
    bool intersect(const glm::vec2 &begin, 
                   const glm::vec2 &end, 
                   const Segment &seg, 
                   glm::vec2 *point, 
                   float *paramU);

    glm::vec2 mPos;
    std::vector<Intersection> mIntersections;

};


#endif

