#include "utility.hh"
#include <random>
#include <math.h>
#include <iostream>

namespace Utility {

float map(const float value,
          const float min1, const float max1,
          const float min2, const float max2) {

    return min2 + ((max2 - min2) / (max1 - min1)) * (value - min1);
}

int random(const int &min, const int &max) {
    return  min + rand() % (max-min+1);
}


void split(std::vector<std::string> &vec,
           std::string s,
           const std::string &separator,
           bool removeEmpty,
           bool removeSpaces) {

    std::string::size_type begin = 0;
    std::string::size_type end = 0;
    std::string item = "";

    while(end != std::string::npos) {

        end = s.find(separator, begin);
        item = s.substr(begin, end-begin);

        if (item != "" || !removeEmpty) {
            
            if (removeSpaces) {
                for (auto it=item.begin(); it != item.end();) {
                    
                    if (*it == ' ') item.erase(it);
                    else ++it;
                }
            }
            
            vec.push_back(item);
        }
        begin = end + separator.size();
    }

}


int digits(int x) {
    x = abs(x);  
    return (x < 10 ? 1 :   
           (x < 100 ? 2 :   
           (x < 1000 ? 3 :   
           (x < 10000 ? 4 :   
           (x < 100000 ? 5 :   
           (x < 1000000 ? 6 :   
           (x < 10000000 ? 7 :  
           (x < 100000000 ? 8 :  
           (x < 1000000000 ? 9 :  
           10)))))))));  
}

} // utility

