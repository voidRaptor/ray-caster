#ifndef WINDOW_HH
#define WINDOW_HH

#include <string>
#include <SDL2/SDL.h>


// TODO: create separate wrapper for SDL_Renderer?


/**
* @brief class for accessing the window created by SDL
*/
class Window {

public:
    Window();
    ~Window();


    /**
    * @brief get pointer to SDL rendering context used to render objects
    */
    SDL_Renderer* getRenderer();


    
    /**
    * @brief get pointer to window surface, which can be used for direct pixel access
    */
    SDL_Surface* getWindowSurface();


    /**
    * @brief update fps counter located in the title
    *
    * @param deltaTime, time between current and previous frame
    */
    void updateFrameCounter(const double &deltaTime);


    /**
    * @brief clear screen with color CLEAR_COLOR
    */
    void clearScreen();


    /**
    * @brief render drawn elements into framebuffer
    */
    void updateScreen();

    bool isInited();

    const int WIDTH;
    const int HEIGHT;
    

private:
    bool init();


    bool mInited;
    
    SDL_Window* mWindow;
    SDL_Renderer* mRenderer;

    // TODO: to config file and class Config
    static const bool ENABLE_VSYNC;
    static const SDL_Color CLEAR_COLOR;
    static const std::string TITLE;

};


#endif
