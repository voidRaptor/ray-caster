#ifndef KEYBOARDINPUT_HH
#define KEYBOARDINPUT_HH

#include "time/stopwatch.hh"
#include <iostream>
#include <SDL2/SDL.h>
#include <glm/vec2.hpp>


/**
 * @brief singleton for handling keyboard input
 * 
 */
class KeyboardInput {

public:
    static KeyboardInput* getInstance();

    
    /**
     * @brief used to detect exit commands (q and window's X)
     * @param e, SDL eventhandler item 
     */
    bool exitPressed();


    /**
    * @brief populate values of param v with movement directions
    *
    * @param v, address to a valid object
    * @note controlled with keys W, A, S and D
    */
    void handleMovement(glm::vec2* v);


    /**
    * @brief change angle according to keyboard input
    *
    * @param angle, angle to be changed
    * @note controlled with keys J and L
    */
    void handleCamera(float &angle);

    
    // prevent other instances
    KeyboardInput(const KeyboardInput&) = delete;
    KeyboardInput(KeyboardInput&&) = delete;
    KeyboardInput& operator=(const KeyboardInput&) = delete;
    KeyboardInput& operator=(KeyboardInput&&) = delete;

private:
    KeyboardInput();
    ~KeyboardInput() = default;
    
    SDL_Event mEvent;
  
};


#endif
