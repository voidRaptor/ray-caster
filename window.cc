#include "window.hh"
#include <iostream>
#include <string>


const bool Window::ENABLE_VSYNC = true;
const SDL_Color Window::CLEAR_COLOR = {0, 0, 0, 255};
const std::string Window::TITLE = "Ray Caster";


Window::Window():
    WIDTH(800), 
    HEIGHT(600),
    mWindow(nullptr),
    mRenderer(nullptr) {
    
        mInited = init();
}


Window::~Window() {


}


SDL_Renderer* Window::getRenderer() {
    return mRenderer;
}


SDL_Surface* Window::getWindowSurface() {
    return SDL_GetWindowSurface(mWindow);
}


void Window::updateFrameCounter(const double &deltaTime) {
    if (deltaTime == 0.0f) return;

    std::string fps = std::to_string( (1.0f / deltaTime) );
    std::string title = TITLE + " | " + fps;

    SDL_SetWindowTitle(mWindow, title.c_str());
}


void Window::clearScreen() {
    SDL_SetRenderDrawColor(mRenderer, 
                           CLEAR_COLOR.r, 
                           CLEAR_COLOR.g, 
                           CLEAR_COLOR.b, 
                           CLEAR_COLOR.a);

    SDL_RenderClear(mRenderer);
}


void Window::updateScreen() {
    SDL_RenderPresent(mRenderer);
}


bool Window::isInited() {
    return mInited;
}


bool Window::init() {

    // init here only what Window actually needs
    if ( SDL_Init(SDL_INIT_EVERYTHING) < 0 ) {
        std::cerr << "SDL_Init: " << SDL_GetError() << "\n";
        return false;
    }

    mWindow = SDL_CreateWindow(TITLE.c_str(),
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               WIDTH,
                               HEIGHT,
                               0);

    if (mWindow == nullptr) {
        std::cerr << "SDL_Window: " << SDL_GetError() << "\n";
        return false;
    }

    uint32_t flags = SDL_RENDERER_ACCELERATED;

    if (ENABLE_VSYNC) flags = SDL_RENDERER_PRESENTVSYNC;

    // TODO: param 2: ext gpu != -1?
    mRenderer = SDL_CreateRenderer(mWindow, -1, flags);

    if (mRenderer == nullptr) {
        std::cerr << "SDL_Renderer: " << SDL_GetError() << "\n";
        return false;
    }



    return true;
}


