#ifndef OBSTACLE_HH
#define OBSTACLE_HH

#include "segment.hh"
#include <vector>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <SDL2/SDL.h>


/**
* @brief representation of shape containing multiple segments
*/
class Obstacle {

public:

    /**
    * @brief define specific x, y, width and height 
    */
    Obstacle(const glm::vec4& dimensions);


    /**
    * @brief build random shape
    *
    * @param center, position for shape's center
    * @param radius, radius for the shape
    * @param segments, number of segments for shape
    */
    Obstacle(const glm::vec2 &center, float radius, unsigned int segments);


    /**
    * @brief randomize all attributes
    */
    Obstacle();


    /**
    * @brief draw segments of the obstacle
    *
    * @param r, SDL rendering context
    */
    void draw(SDL_Renderer* r);


    /**
    * @brief return pointer to segment container
    */
    std::vector<Segment>* getSegments();
    

private:
    void init();
    void initRand();

    glm::vec2 mCenter;
    glm::vec2 mSize;
    std::vector<Segment> mSegments;

};

#endif

